var data = require('../../test_data/Isupport_TestData.js');
var Objects = require(__dirname + '/../../objects/ISupport_Locators.js');
var  robot = require('robotjs');
var clipboard = require('copy-paste');

var  IPage,s,RepPage;
var t;

function initializePageObjects(client, callback) {
    browser = client;

    var locators= browser.page.ISupport_Locators();
    page = locators.section;
    IPage = page.IPage;
    RepPage=page.RepPage;
    callback();
}

module.exports = function() {

    this.Given(/^Once User is logged in to I support application$/, function (browser) {

        var URL;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        URL = data.url_QA;
        // var userDB = data.usersQA[userType];

        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);


            IPage.waitForElementVisible('@ISupportUsername', data.longWait);
            IPage.setValue('@ISupportUsername', data.username);

            IPage.waitForElementVisible('@IsupportPassword', data.longWait);
            IPage.setValue('@IsupportPassword', data.password);
            IPage.click('@buttonLogin', function () {
                browser.timeoutsImplicitWait(30000);
                IPage.waitForElementVisible('@PageTitle', data.longWait);
            });
        });
    });

    this.Given(/^User is logged in to I support rep application$/, function (browser) {
        var t;
        var URL;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        URL = data.url_QARep;
        // var userDB = data.usersQA[userType];

        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);


            IPage.waitForElementVisible('@Repusername', data.longWait);
            IPage.setValue('@Repusername', data.username);

            IPage.waitForElementVisible('@Password', data.longWait);
            IPage.setValue('@Password', data.reppassword);
            IPage.click('@RepLogin');

            // IPage.waitForElementVisible('@RepPage', data.longWait);


        });
    });


    this.Then(/^User navigates to external access$/, function (browser) {


        IPage.waitForElementVisible('@AddButton', data.shortWait)
            .click('@AddButton');
        IPage.waitForElementVisible('@ExternalAccess', data.shortWait)
            .click('@ExternalAccess');
        browser.useXpath().getText('(//div[@class="col-md-9"])[3]', function (result) {
            t = result.value;
            console.log(t);

        });

        IPage.waitForElementVisible('@Company', data.shortWait)
            .click('@Company');
        IPage.waitForElementVisible('@CompOptions', data.shortWait)
            .click('@CompOptions');
        IPage.waitForElementVisible('@TicketDetails', data.shortWait)
            .click('@TicketDetails');
        IPage.waitForElementVisible('@AccessModi', data.shortWait)
            .click('@AccessModi');
        IPage.waitForElementVisible('@AddAccess', data.shortWait)
            .click('@AddAccess')
            .waitForElementVisible('@AddAccess', data.shortWait)
            .click('@AddAccess')
            .waitForElementVisible('@BWAccess', data.shortWait)
            .click('@BWAccess')
            .waitForElementVisible('@New', data.shortWait)
            .click('@New')
            .waitForElementVisible('@Production', data.shortWait)
            .click('@Production')
            .waitForElementVisible('@Upload', data.shortWait)
            .click('@Upload')
            .waitForElementVisible('@Approval', data.shortWait)
            .click('@Approval')
            .waitForElementVisible('@Leader', data.shortWait)
            .click('@Leader')
            .waitForElementVisible('@Comments', data.shortWait)
            .setValue('@Comments', data.text)
            .waitForElementVisible('@Save', data.longWait)
            .click('@Save');
    });


    this.Then(/^User enters the ticket number in search box$/, function (browser) {
        browser.pause(6000).frame(0);
        RepPage.waitForElementVisible('@Search', data.shortWait)
            .setValue('@Search', t);
        RepPage.waitForElementVisible('@SearchButton', data.shortWait)
            .click('@SearchButton');
        browser.pause(5000);
        RepPage.waitForElementVisible('@Changes', data.shortWait)
            .click('@Changes');
        RepPage.waitForElementVisible('@Highlight', data.shortWait)
            .click('@Highlight');
        browser.windowHandles(function (result) {
            var newWindow;
            newWindow = result.value[1];
            this.switchWindow(newWindow);

            RepPage.waitForElementVisible('@Action', data.longWait)
                .click('@Action')
                .waitForElementVisible('@ActionApprove', data.longWait)
                .click('@ActionApprove')
                .waitForElementVisible('@RepComment', data.longWait)
                .setValue('@RepComment', data.text)
                .waitForElementVisible('@Submit', data.longWait)
                .click('@Submit');

            browser.acceptAlert();
            RepPage.waitForElementVisible('@ReloadWindow', data.longWait)
                .click('@ReloadWindow');
            browser.timeoutsImplicitWait(30000);


            RepPage.waitForElementVisible('@Action', data.longWait)
                .click('@Action')
                .waitForElementVisible('@ActionApprove', data.longWait)
                .click('@ActionApprove')
                .waitForElementVisible('@RepComment', data.longWait)
                .setValue('@RepComment', data.text)
                .waitForElementVisible('@Submit', data.longWait)
                .click('@Submit');
            browser.acceptAlert();
            browser.closeWindow();


        });
    });


    this.Then(/^User searches for raised ticket in REP$/, function (browser) {


        browser.windowHandles(function (result) {
            var newWindow;
            newWindow = result.value[0];
            this.switchWindow(newWindow);
        });
        browser.pause(3000).frame(0);
        RepPage.click('@Highlight1');


    });


    this.Then(/^User verifies that ticket is approved$/, function (browser) {
        browser.windowHandles(function (result) {
            var newWindow;
            newWindow = result.value[1];
            this.switchWindow(newWindow);
            browser.timeoutsImplicitWait(30000);
            browser.pause(5000);
            RepPage.waitForElementVisible("@ChildTicket", data.longWait);


        });
    });
}







