var seleniumServer = require('selenium-server');
var chromedriver=require('chromedriver');
var ieDriver = require('iedriver');
var firefoxDriver = require('geckodriver');
var data1 = require('./test_data/HRA_TestData.js');

require('nightwatch-cucumber')({
    supportFiles: ['./utils/TestExecListener.js'],
    stepTimeout: 1000000,
    defaultTimeoutInterval: 60000,
    nightwatchClientAsParameter: true
});

module.exports = {
    output_folder: 'reports',
    custom_commands_path: '',
    custom_assertions_path: '',
    page_objects_path: "objects",
    live_output: false,
    disable_colors: false,
    test_workers: {
        enabled: false,
        workers: 3
    },
    selenium: {
        start_process: true,
        server_path: seleniumServer.path,
        host: '127.0.0.1',
        port: 5555,
        cli_args: {
            "webdriver.chrome.driver" :chromedriver.path,
            'webdriver.ie.driver': 'C:\\hrax_smoke\\usben_hraxeos\\IEDriverServer.exe',
            'webdriver.firefox.driver': firefoxDriver.path
        }
    },

    test_settings: {
        default: {
            launch_url: "http://localhost",
            page_objects_path: "objects",
            selenium_host: "127.0.0.1",
            selenium_port: 5555,
            silent: true,
            disable_colors: false,
            screenshots: {
                enabled: true,
                on_failure: true,
                on_error: true,
                path: 'screenshots'
            },
            desiredCapabilities: {
                browserName: data1.NameofThebrowser,
                javascriptEnabled: true,
                acceptSslCerts: true,
            },
            iphone: {
                desiredCapabilities: {
                    "browserName": "Safari",
                    appiumVersion:'1.6.3',
                    platformName: "iOS",
                    platformVersion: "9.2",
                    deviceName: "iPhone 6s simulator"
                }
            }
        },
        chrome: {
            desiredCapabilities: {
                browserName: "chrome",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        firefox: {
            desiredCapabilities: {
                browserName: "firefox",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ie: {
            desiredCapabilities: {
                browserName: "internet explorer",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },


        /*
         default: {
         launch_url: "http://ondemand.saucelabs.com:80",
         selenium_port: 80,
         selenium_host: "ondemand.saucelabs.com",
         silent: true,
         username: "kingthev",
         access_key: "ee3bb843-1aee-40f8-8858-c25c377db05a",
         screenshots: {
         enabled: false,
         path: "",
         },
         globals: {
         waitForConditionTimeout: 10000,
         },
         desiredCapabilities: {
         browserName: "iphone",
         }
         },

         iphone: {
         desiredCapabilities: {
         browserName:  'Safari',
         appiumVersion: '1.6.3',
         deviceName: 'iPhone 6s Simulator',
         deviceOrientation: 'portrait',
         platformVersion: '10.0',
         platformName: 'iOS'
         }
         },

         android: {
         desiredCapabilities: {
         browserName: 'Browser',
         appiumVersion: '1.5.3',
         deviceName: 'Samsung Galaxy S4 Emulator',
         deviceOrientation: 'portrait',
         platformVersion: '4.4',
         platformName: 'Android'
         }
         },

         chrome: {
         desiredCapabilities: {
         browserName: "chrome",
         platform: "OS X 10.11",
         version: "47",
         },
         },

         ie11: {
         desiredCapabilities: {
         browserName: "internet explorer",
         platform: "Windows 10",
         version: "11.0",
         },
         },*/

    }

}

