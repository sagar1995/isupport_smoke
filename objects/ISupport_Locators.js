module.exports = {
    sections: {

        //HRA XEOS locators
        IPage: {
            selector: 'body',
            elements: {
                ISupportUsername: {locateStrategy: 'xpath', selector: "//input[@name='UserName']"},
                IsupportPassword: {locateStrategy: 'xpath', selector: "//input[@name='Password']"},
                buttonLogin: {
                    locateStrategy: 'xpath',
                    selector: "//input[@class='btn btn-default LockoutSubmitButton']"
                },
                PageTitle: {locateStrategy: 'xpath', selector: "//div[@id='sb-site']"},
                Repusername: {locateStrategy: 'xpath', selector: "//input[@name='uxInput_UserName']"},
                Password: {selector: "input[id='uxInput_Password']"},
                RepLogin: {selector: "input[id='uxButton_Login']"},
               // Repage: {locateStrategy: 'xpath', selector: "//div[@id='SocialDashboard']"},
                AddButton: {locateStrategy: 'xpath', selector: "(//a[contains(text(),'Application Access')])[1]"},
                ExternalAccess: {locateStrategy: 'xpath', selector: "//a[@data-label='external access']"},
                // TicketNumber: {locateStrategy: 'xpath',selector: "(//label[@class='workItemFieldNew control-label col-md-12 form-control-static'])[2]"},
                Company: {locateStrategy: 'xpath', selector: "//select[@id='_EntityFieldSelect_uxSelect_29']"},
                CompOptions: {locateStrategy: 'xpath',selector: "//select[@id='_EntityFieldSelect_uxSelect_29']/option[7]"},
                TicketDetails: {locateStrategy: 'xpath', selector: "(//span[@class='tab-text'])[2]"},
                AccessModi: {locateStrategy: 'xpath', selector: "//select[@id='cf_3320']"},
                AddAccess: {locateStrategy: 'xpath', selector: "//select[@id='cf_3320']/option[2]"},
                BWAccess: {locateStrategy: 'xpath', selector: "//input[@id='cf_3323_0']"},
                New: {locateStrategy: 'xpath', selector: "//input[@id='cf_3400_0']"},
                Production: {locateStrategy: 'xpath', selector: "//input[@id='cf_3402_0']"},
                Upload: {locateStrategy: 'xpath', selector: "//input[@id='cf_3404_0']"},
                Approval: {locateStrategy: 'xpath', selector: "//select[@id='cf_3344']"},
                Leader: {locateStrategy: 'xpath', selector: "//select[@id='cf_3344']/option[4]"},
                Comments: {locateStrategy: 'xpath', selector: "//textarea[@id='cf_3346']"},
                Save: {locateStrategy: 'xpath', selector: "//div[@title='Save']"}
            }},
        RepPage: {
            selector: 'body',
            elements: {
                Search: {selector: "input#uxGlobalSearch_uxTextBox_GlobalSearch"},
                SearchButton: {selector: "i[class='icon-search']"},
                Changes: {locateStrategy: 'xpath',selector: "(//span[@class='rtsTxt'])[2]"},
                Highlight: {locateStrategy: 'xpath',selector: "//a[@class='extendedGridViewHighlightableField Number layout-float-left selectChangeLink layout-font-weight-bold']"},
                Highlight1: {locateStrategy: 'xpath',selector: "//span[@class='highlight']"},
                Action: {locateStrategy: 'xpath',selector: "//select/option[contains(text(),'Select an Action')]"},
                ActionApprove: {locateStrategy: 'xpath',selector: "(//select/option[contains(text(),'Approve')])[1]"},
                RepComment: {locateStrategy: 'xpath',selector: "//textarea[@name='uxTextBox_ApprovalComment']"},
                Submit: {locateStrategy: 'xpath',selector: "//input[@id='uxButton_SubmitApprovals']"},
                ReloadWindow: {locateStrategy: 'xpath',selector: "//input[@id='uxButton_ReloadWindow']"},
               // SavenExit: {locateStrategy: 'xpath',selector: "//input[@id='uxButton_ReloadWindow']"},
               // ViewRelated: {locateStrategy: 'xpath',selector: "(//span[@class='rrbTextContent'])[15]"},
                ChildTicket: {locateStrategy:'xpath',selector: "(//select[contains(.,'Approved')])[1]"}







            }
        },

    }
};
